#!/usr/bin/python3

entries=[]
with open("compile_commands.json", "r") as f:
    entries = eval(f.read())

idx = 0
with open("compile_commands.json.Makefile", "w") as o:
    o.write("all: all_helper\n")
    o.write("\n")
    for entry in entries:
        file = entry["file"]
        command = entry["command"]
        directory = entry["directory"]
        objfile = False
        for p in command.split(" "):
            if objfile:
                objfile = p
                break
            if p == "-o":
                objfile = True
        pre = "targ_" + str(idx)
        idx = idx + 1
        o.write(pre + "_dir=" + directory + "\n")
        o.write(pre + "_cmd=" + command + "\n")
        o.write(pre + "_file=" + file + "\n")
        o.write(pre + "_obj=" + objfile + "\n")
        o.write(pre + ":\n")
        o.write(
            "\tcd $(" +pre + "_dir) && " + 
            "[ $(" +pre + "_obj) -nt $(" +pre + "_file) ] || " + 
            "$(" +pre + "_cmd)\n")
        o.write("\n")
        o.write("\n")
    o.write("all_helper:")
    for i in range(idx):
        o.write(" targ_" + str(i))
    o.write("\n")
